import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SubnetCalculator {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter IP address (e.g., 192.168.1.1): ");
        String ipAddress = scanner.nextLine();

        if (isValidIP(ipAddress)) {
            System.out.print("Enter subnet mask (e.g., 255.255.255.0): ");
            String subnetMask = scanner.nextLine();

            if (isValidIP(subnetMask)) {
                String binaryIP = convertIPToBinary(ipAddress);
                String binarySubnetMask = convertIPToBinary(subnetMask);
                String networkAddress = calculateNetworkAddress(binaryIP, binarySubnetMask);
                String broadcastAddress = calculateBroadcastAddress(binaryIP, binarySubnetMask);
                int numHosts = calculateNumHosts(binarySubnetMask);

                System.out.println("IP Address: " + ipAddress);
                System.out.println("Subnet Mask: " + subnetMask);
                System.out.println("Binary IP Address: " + binaryIP);
                System.out.println("Binary Subnet Mask: " + binarySubnetMask);
                System.out.println("Network Address: " + networkAddress);
                System.out.println("Broadcast Address: " + broadcastAddress);
                System.out.println("Number of Hosts: " + numHosts);
            } else {
                System.out.println("Invalid subnet mask.");
            }
        } else {
            System.out.println("Invalid IP address.");
        }

        scanner.close();
    }

    public static boolean isValidIP(String ip) {
        String regex = "^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(ip);
        return matcher.matches();
    }

    public static String convertIPToBinary(String ip) {
        String[] octets = ip.split("\\.");
        StringBuilder binaryIP = new StringBuilder();
        for (String octet : octets) {
            int decimal = Integer.parseInt(octet);
            String binary = Integer.toBinaryString(decimal);
            binaryIP.append(String.format("%8s", binary).replace(' ', '0'));
        }
        return binaryIP.toString();
    }

    public static String calculateNetworkAddress(String binaryIP, String binarySubnetMask) {
        StringBuilder networkAddress = new StringBuilder();
        for (int i = 0; i < binaryIP.length(); i++) {
            char ipBit = binaryIP.charAt(i);
            char subnetMaskBit = binarySubnetMask.charAt(i);
            if (ipBit == '1' && subnetMaskBit == '1') {
                networkAddress.append('1');
            } else {
                networkAddress.append('0');
            }
        }
        return networkAddress.toString();
    }

    public static String calculateBroadcastAddress(String binaryIP, String binarySubnetMask) {
        StringBuilder broadcastAddress = new StringBuilder();
        for (int i = 0; i < binaryIP.length(); i++) {
            char ipBit = binaryIP.charAt(i);
            char subnetMaskBit = binarySubnetMask.charAt(i);
            if (subnetMaskBit == '1') {
                broadcastAddress.append(ipBit);
            } else {
                broadcastAddress.append('1');
            }
        }
        return broadcastAddress.toString();
    }

    public static int calculateNumHosts(String binarySubnetMask) {
        int numHostBits = binarySubnetMask.replace("0", "").length();
        return (int) Math.pow(2, numHostBits) - 2; // Subtract 2 for network and broadcast addresses
    }
}
