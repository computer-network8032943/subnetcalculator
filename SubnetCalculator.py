import ipaddress

def subnet_bitmap_to_custom_format(subnet_bitmap):
    binary_subnet_bitmap = '1' * subnet_bitmap + '0' * (32 - subnet_bitmap)
    custom_format = f'110{binary_subnet_bitmap[3:11]}.{binary_subnet_bitmap[11:19]}.{binary_subnet_bitmap[19:27]}.{binary_subnet_bitmap[27:]}'
    return custom_format

def main():
    ip_input = input("Enter IP address: ")
    subnet_mask_input = input("Enter Subnet Mask: ")

    try:
        ip = ipaddress.IPv4Address(ip_input)
        subnet_mask = ipaddress.IPv4Network(f"{ip}/{subnet_mask_input}", strict=False)
        
        network_address = subnet_mask.network_address
        broadcast_address = subnet_mask.broadcast_address
        subnet_id = network_address
        subnet_bitmap = subnet_mask.prefixlen

        subnet_bitmap_custom_format = subnet_bitmap_to_custom_format(subnet_bitmap)

        host_address_range_start = network_address + 1
        host_address_range_end = broadcast_address - 1

        print(f"IP Address: {ip}")
        print(f"Subnet Mask: {subnet_mask.netmask}")
        print(f"Network Address: {network_address}")
        print(f"Broadcast Address: {broadcast_address}")
        print(f"Subnet ID (Decimal): {subnet_id}")
        print(f"Subnet Bitmap (Custom Format): {subnet_bitmap_custom_format}")
        print(f"Host Address Range: {host_address_range_start} - {host_address_range_end}")

    except ipaddress.AddressValueError:
        print("Invalid IP address or Subnet Mask.")

if __name__ == "__main__":
    main()
